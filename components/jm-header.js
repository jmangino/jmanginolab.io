const template = document.createElement('template');
template.innerHTML=`
<style>
	:host{
		flex-wrap: wrap;
	}

	.admin{
		display:none!important;
	}

	:host-context(.admin) .admin{
		display:initial!important;
	}

	body.admin .admin{
		display:initial!important;
	}

  header nav{
  	background-color:#151515;
  	width:100%;
  	border-bottom:black 2px solid;
  }

  header {
    width: 100%;
    background-color: #000;
		font-family: sans-serif;
  }

  nav a{
    text-decoration: none;
    color:#c0c0c0 !important;
    padding:10px 15px !important;
  }

  header nav a:hover{
    color: white !important;
    background-color:rgba(0,0,0,0.70);
  }

  header img{
    display:inline-block;
    width:8vw;
  }

  header span.title{
  	font-size:6vw;
  	font-weight:bold;
  	color: white;
  }
  /*Jmenu*/
  /* JMenu 1.0 RC1 | MIT License | https://github.com/jamesjohnson280/JMenu */
  .jmenu{background:#252525;box-shadow:1px 1px 3px 0 rgba(0,0,0,.5);box-sizing:border-box;line-height:1}input.jm-menu-btn,input[type=checkbox].jm-menu-btn~.jm-collapse{display:none}input[type=checkbox]:checked.jm-menu-btn~.jm-collapse{display:block}label.jm-menu-btn{color:#959595;cursor:pointer;display:block;padding:16px 32px}label.jm-menu-btn:hover{color:#fff}.jm-collapse{border-top:1px #959595 solid}.jmenu li,.jmenu ul{list-style:none;margin:0;padding:0}.jmenu a{color:#959595;display:inline-block;padding:16px 32px;text-decoration:none}.jm-dropdown:hover a,.jmenu a:hover{color:#fff}.jmenu ul ul{display:none}.jm-dropdown:hover ul{display:block}.jm-dropdown ul{background:#fff;padding:0}.jm-dropdown ul a,.jm-dropdown:hover ul a{color:#0072bc}.jm-dropdown ul a:hover,.jm-dropdown:hover ul a:hover{color:#000}.jm-dropdown ul ul{border-bottom:1px #ccc solid;border-top:1px #ccc solid;box-shadow:none;margin-bottom:16px;max-width:100%;position:relative}.jm-icon-dropdown{border:solid #959595;border-width:0 2px 2px 0;display:inline-block;margin:0 0 3px 8px;padding:3px;transform:rotate(45deg)}li:hover .jm-icon-dropdown{border-color:#fff}@media (min-width:768px){.jmenu li{display:inline-block}.jmenu a{padding:16px}.jm-dropdown{position:relative}.jm-dropdown li a{display:block;padding:8px 16px;white-space:nowrap}.jm-dropdown ul{box-shadow:1px 1px 3px 0 rgba(0,0,0,.5);padding:8px 0;position:absolute;min-width:100%}input.jm-menu-btn,label.jm-menu-btn{display:none}.jm-collapse,input[type=checkbox].jm-menu-btn~.jm-collapse{display:block}}
  /*END Jmenu*/

</style>
<header>
	<span class='title'>Joe Mangino</span>
	<nav class ='jmenu'>
	<label for='menu-btn' class='jm-menu-btn'><i class='fas fa-bars'></i></label>
  <input type='checkbox' id='menu-btn' class='jm-menu-btn'>
		<!-- can the class apply directly to the anchor tag? -->
		<ul class = 'jm-collapse'>
			<li><a href='/index.html'>Home</a></li>
			<li><a href='/about.html'>About</a></li>
		</ul>
	</nav>
</header>
`;

ShadyCSS.prepareTemplate(template, 'jm-header')
window.customElements.define('jm-header', class extends HTMLElement{
	constructor(){
		super();
		this.attachShadow({mode: 'open'});
		ShadyCSS.styleElement(this);
		this.shadowRoot.appendChild(template.content.cloneNode(true));
		this.loadResources();
	}

	connectedCallback(){}

	loadResources(){
    //load fontawesome
		let link = document.createElement('link');
		link.setAttribute('rel','stylesheet');
		link.setAttribute('href','https://use.fontawesome.com/releases/v5.2.0/css/all.css');
		link.setAttribute('integrity','sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ');
		link.setAttribute('crossorigin','anonymous');
		//append so shadowroot has access
		this.shadowRoot.appendChild(link);

  }
});
