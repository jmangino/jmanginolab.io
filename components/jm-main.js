const template = document.createElement('template');
template.innerHTML=`
<style>
	:host{
		flex-wrap: wrap;
	}

	@media only screen and (min-width:768px){
	  footer{
	    margin-left: 2vw;
	    width: 720px;
	  }
	}

	nav a:visited{
	  color:inherit;
	}

	nav a{
	  text-decoration: none;
	  padding:10px 15px !important;
	}

	footer nav a:hover{
	  background-color:rgba(0,0,0,0.20);
	}

	footer{
	  background:rgb(215,215,215);
	  padding: 1vw 3vw;
	}

	footer p {
	   color: rgba(0,0,0,0.4);  font-size: 13px;
	}

	mdc-header{
		width:100%;
	}

	.text-center{
	  text-align: center;
	}
</style>
<jm-header></jm-header>
<slot name ="content" class="container"></slot>
<footer class = 'text-center'>

	<p>
	Copyright © 2019 Joseph Mangino
	<br>
	All rights reserved.
	</p>
</footer>
`;
ShadyCSS.prepareTemplate(template, 'jm-main')
window.customElements.define('jm-main', class extends HTMLElement{

	constructor(){
		super();
		this.attachShadow({mode: 'open'});
		ShadyCSS.styleElement(this);
		this.shadowRoot.appendChild(template.content.cloneNode(true));
		this.loadResources();
	}

	connectedCallback(){ }

	loadResources(){
		//load fontawesome
		let link = document.createElement('link');
		link.setAttribute('rel','stylesheet');
		link.setAttribute('href','https://use.fontawesome.com/releases/v5.2.0/css/all.css');
		link.setAttribute('integrity','sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ');
		link.setAttribute('crossorigin','anonymous');
		//append both so shadowroot has access, but sahdow root wasn't loading @import fonts correctly
		document.head.appendChild(link);
		this.shadowRoot.appendChild(link.cloneNode(true));
	}

});
